chardet==4.0.0
cow_csvw==1.21
iribaker==0.2
isodate==0.6.1
Jinja2==3.0.3
Js2Py==0.71
MarkupSafe==2.1.4
pyjsparser==2.7.1
pyparsing==3.1.1
pytz==2021.3
pytz-deprecation-shim==0.1.0.post0
PyYAML==6.0
rdflib==6.0.2
rfc3987==1.3.8
six==1.16.0
tzdata==2023.4
tzlocal==4.1
unicodecsv==0.14.1
Werkzeug==2.0.2
